# Live demo

![live demo gif](live-demo.gif)
# Cloning Repo

➤ Clone Repo with git link

```bash
git clone git@gitlab.com:TavaresDylan/flask_plant_vision.git
```

➤ Setup virtual environment

```bash
python3 -m venv venv
```

➤ Activate Virtual env

```bash
source ./venv/bin/activate
```

➤ Upgrade pip in virtual env 
```bash
pip install --upgrade pip
```

➤ Install all libraries required

```bash
pip -r requirements.txt
```

➤ Move into /app directory

```bash
cd app/
````

➤ Export Flask app variable

```bash
export FLASK_APP = "main.py"
```

➤ If you want you can activate devmode 

```bash
export FLASK_DEV="development"
```

➤ Modify headers.py.sample (to connect your own azure cognitive vision client)

```bash
# rename
mv headers.py.sample headers.py
# write your keys and ids into the file
sudo nano headers.py
```

➤ Finally run server

```bash
flask run
```

# Trello d'organisation

➤ [Lien vers le trello](https://trello.com/b/2l5ELZ61)

# Some Datas

➤ **[SELECTED dataset](https://www.tensorflow.org/datasets/catalog/plant_leaves)**

➤ [Plant Village (antoine)](https://www.kaggle.com/emmarex/plantdisease)

➤ [New plant disease Kaagle](https://www.kaggle.com/vipoooool/new-plant-diseases-dataset)

# API Code snippets

```python
########### Python 3.2 #############
import http.client, urllib.request, urllib.parse, urllib.error, base64

headers = {
    # Request headers
    'Content-Type': 'application/octet-stream',
    'Prediction-key': '{subscription key}',
}

params = urllib.parse.urlencode({
    # Request parameters
    'application': '{string}',
})

try:
    conn = http.client.HTTPSConnection('southcentralus.api.cognitive.microsoft.com')
    conn.request("POST", "/customvision/v3.1/Prediction/{projectId}/classify/iterations/{publishedName}/image?%s" % params, "{body}", headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))

####################################
```

<hr>

```python
     ENDPOINT = "https://northeurope.api.cognitive.microsoft.com/"
     PROJECT_ID = "my project id"
     prediction_key = "my prediction key"
        
     prediction_credentials = ApiKeyCredentials(in_headers={"Prediction-key": prediction_key})
     predictor = CustomVisionPredictionClient(ENDPOINT, prediction_credentials)
        
     image_location = "/tmp/123.jpg"
     publish_iteration_name = "Iteration2"
        
     with open(image_location, "rb") as image_contents:
         results = predictor.classify_image(
             PROJECT_ID, publish_iteration_name, image_contents.read())
```

## Ressources API

➤ [Repo d'exmple github](https://github.com/syedsohaibuddin/Pyday-customvision-Webapp)

➤ [Tutorial Medium C#](https://medium.com/@syed.sohaib/creating-a-classifier-using-custom-vision-api-and-consuming-it-in-a-flask-application-2ba580fd32e7)

➤ [Microsoft API documentation](https://southcentralus.dev.cognitive.microsoft.com/docs/services/Custom_Vision_Prediction_3.1/operations/5eb37d24548b571998fde5f3)

## Errors ressources

➤ [No module named](https://stackoverflow.com/questions/63175483/python-modulenotfounderror-no-module-named-azure-cognitiveservices)

➤ [Invalid Iteration](https://docs.microsoft.com/en-us/answers/questions/290457/predictorclassify-image-failed-with-error-34invali.html)

*Made by [Dylan Tavarès](https://www.linkedin.com/in/dylan-tavar%C3%A8s-727b23187/) with* ❤️