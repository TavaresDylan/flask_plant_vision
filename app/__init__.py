# Importation du framework flask
from flask import Flask
# Import des variables de configuration
from config import Config
# Importation de bootstrap
from flask_bootstrap import Bootstrap
# Déclaration de l'application
app = Flask(__name__,template_folder="templates",static_folder="static")
app.config.from_object(Config)
# Déclaration de l'utilisation de bootstrap
Bootstrap(app)
# Import du module de gestion des routes de flask
from app import routes
