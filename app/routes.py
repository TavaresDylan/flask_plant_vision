from app import app
from flask import render_template
from flask import request
import config
from PIL import Image
import os

from .headers import *

# Fonction de récupération de l'extension de fichier
def allowed_file(filename):
   return '.' in filename and \
         filename.rsplit('.', 1)[1].lower() in app.config["ALLOWED_IMAGE_EXTENSIONS"]

@app.route('/')
def index():
   return render_template("index.html",title="PlantsCare || Home")

@app.route('/about')
def about():
   return render_template("about.html",title="PlantsCare || About")

@app.route('/predict', methods=["GET","POST"])
def predict():
   # Si la requête est une method POST
   if request.method == "POST":
      # Déclaration d'une variable correspondante à l'image d'entrée
      image = request.files["image"]
      # Si il n'y a pas de fichier soumis
      if image.filename == '':
         no_file = "Error no file submited"
         return render_template("predict.html", title="PlantsCare || Make Prediction",no_file=no_file)
      # Si la requête contient un fichier
      if allowed_file(image.filename):
         # Déclaration d'une variable correspondante à l'image d'entrée
         image = request.files["image"]
         # Enregistrement de l'image depuis le chemin spécifié dans le fichier config.py avec son nom original
         image.save(os.path.join(app.config["IMAGE_UPLOADS"], image.filename))
         # Ouverture de l'image avec la librairie PIL
         img_load = Image.open(app.config["IMAGE_UPLOADS"]+image.filename)
         # Récupération des des informations de connection à l'api
         prediction_credentials = ApiKeyCredentials(in_headers={"Prediction-key": prediction_key})
         predictor = CustomVisionPredictionClient(ENDPOINT, prediction_credentials)
         # ouverture de l'image locale et prédiction de classification
         with open(app.config["IMAGE_UPLOADS"]+image.filename, "rb") as image_contents:
               results = predictor.classify_image(
                  projectId, publish_iteration_name, image_contents.read())
               result=""
               # Création d'un tableau pour les pourcentages
               percent = []
               # Bloucle pour acceder aux infos de la prédiction
               for prediction in results.predictions:
                  percent.append("{0:.0f}% ".format(prediction.probability * 100))
                  result += "\t\t\n" + prediction.tag_name + " : {0:.2f}% ".format(prediction.probability * 100)
         # Retourne la vue si envoie d'un POST avec les informations
         return render_template("predict.html",title="PlantsCare || Make Prediction" ,image=img_load, img_name=image.filename,img_path=app.config["IMAGE_UPLOADS"]+image.filename, result=result,percent_disease=percent[0],percent_healthy=percent[1])
      # Sinon afficher l'extension n'est pas autorisé
      else:
         not_allowed = "File extension not allowed"
         return render_template("predict.html", title="PlantsCare || Make Prediction",not_allowed=not_allowed)
   # Renvoie la vue dans le cas d'une demande simple d'affichage de la page
   return render_template("predict.html",title="PlantsCare || Make Prediction")

@app.route('/credits')
def credits():
   return render_template("credits.html",title="PlantsCare || Credits")