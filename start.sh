#!/bin/bash

if [ -e venv ]; then
    source ./venv/bin/activate
    cd app/
    export FLASK_APP = "main.py"
    export FLASK_DEV = "development"
    flask run
else
    python3 -m venv venv
    pip install -r requirements.txt
    source ./venv/bin/activate
    cd app/
    export FLASK_APP = "main.py"
    export FLASK_DEV = "development"
    flask run
    echo "app is now running you just need to complete app/headers.sample.py and rename it into headers.py"
fi
exit 0